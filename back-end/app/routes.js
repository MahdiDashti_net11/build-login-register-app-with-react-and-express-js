const authRouter = require('./components/auth/router')
const initRouter=require('./components/init/router')
module.exports = (app) => {
    app.use('/api/v1/auth',authRouter)
    app.use('/api/v1/init',initRouter)
}