const db = require('../../../db/connections/sequelize')
const { hashPassword,comparePassword } = require('../../services/Hash')
exports.register = async (params) => {
    params.password = hashPassword(params.password)
    const users = await db.user.create(params)
    return users
}





exports.login = async (email, password) => {
    const user = await db.user.findOne({ where: { email } })
    if (!user) {
        return false;
    }

    if (comparePassword(password, user.password)) {
        return user;
    }

    return false;
}
