const Sequelize = require('sequelize')
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'mysql',

    define: {
        charset: 'utf8',
        collate: 'utf8_persian_ci',
        timestamps: true
    },
    logging: false,
    timezone: '+03:30',
})

const db = {}
sequelize.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

db.user = sequelize.import('../models/sequlize/user.js')
sequelize.sync()
module.exports = db